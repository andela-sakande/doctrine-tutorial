<?php

require_once "bootstrap.php";

$id = $argv[1];
$newName = $argv[2];

$product = $entityManager->find('Product', $id);

if ($product === null) {
  echo "No product found with that ID. \n";
  exit(1);
}

$product->setName($newName);
echo "Updated successfully";
$entityManager->flush();
